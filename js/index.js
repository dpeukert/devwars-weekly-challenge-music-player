var old_url = "";
var old_track = "";
var old_artist = "";

var audio = $("audio")[0];

var song_change_count = 0;

/* FOR A POSSIBLE VISUALIZER
var audiocontext = new AudioContext;
var source = audiocontext.createMediaElementSource(audio);
var analyser = audiocontext.createAnalyser();
source.connect(analyser);
analyser.connect(audiocontext.destination);
*/

var interval;

// polyfill for non ES6 browsers
Math.trunc = Math.trunc || function(x) {
  return x < 0 ? Math.ceil(x) : Math.floor(x);
}

function leading_zero(num) {
    return (num < 10) ? ("0" + num) : num;
}

function updatetime() {
    var currenttime = audio.currentTime;

    var minutes = Math.trunc(currenttime / 60);
    var seconds = Math.trunc(currenttime % 60);

    if(minutes >= 60) {
        minutes = Math.trunc(minutes / 60) + ":" + leading_zero(Math.trunc(minutes % 60));
    }

    $("#current").html(minutes + ":" + leading_zero(seconds))

    $("progress").val(audio.currentTime);
}

function callback() {
    $("#volume input").triggerHandler("input");
    $("#loading").addClass("hidden");
    window.URL.revokeObjectURL(old_url);
    var duration = audio.duration;

    var minutes = Math.trunc(duration / 60);
    var seconds = Math.trunc(duration % 60);

    if(minutes >= 60) {
        minutes = Math.trunc(minutes / 60) + ":" + leading_zero(Math.trunc(minutes % 60));
    }

    $("#total").html(minutes + ":" + leading_zero(seconds));

    $("#seek, progress").attr("max", duration);
}

function volume(keycode) {
    var val = parseFloat($("#volume input").val());
    if(keycode == 38 && val < 1) $("#volume input").val(val + 0.1);
    if(keycode == 40 && val > 0) $("#volume input").val(val - 0.1);
    $("#volume input").triggerHandler("input");
}

$("#volume input").on("input change",function() {
    audio.volume = $(this).val();
});

if (audio.readyState >= 1) callback(); // if loadedmetadata fired before we started listening to the event

$("audio").on("loadedmetadata", callback);

$("audio").on("ended", function() {
    if (!audio.loop) {
        audio.currentTime = 0;
        $("#playpause").removeClass("pause");
    }
});

$("audio").on("error", function() {
    if(audio.error.code == 4) {
        $("audio").attr('src',old_url);
        $("#track").html(old_track);
        $("#artist").html(old_artist);
        if(song_change_count == 1) {
            $("#albumart").removeClass("unknown");
            song_change_count = 0;
        }
    }
});

$("#seek").on("input change",function() {
    audio.currentTime = $(this).val();
    updatetime();
});

$("#playpause").click(function() {
    if (audio.paused) {
        audio.play();
        $("#playpause").addClass("pause");
        interval = setInterval(updatetime, 100);
    } else {
        audio.pause();
        $("#playpause").removeClass("pause");
        clearInterval(interval);
    }
});

$("#repeat").click(function() {
    audio.loop = (audio.loop) ? false : true;
    $(this).toggleClass("active");
});

$("#rewind").click(function() {
    audio.currentTime -= 10;
    updatetime();
});

$("#forward").click(function() {
    audio.currentTime += 10;
    updatetime();
});

$(window).keydown(function(e){
    if(e.keyCode == 32 || e.keyCode == 0) { // space
        $("#playpause").click();
    } else if(e.keyCode == 37) { // left
        $("#rewind").click().addClass("active");
    } else if(e.keyCode == 39) { // right
        $("#forward").click().addClass("active");
    } else if(e.keyCode == 38 || e.keyCode == 40) { // up and down
        volume(e.keyCode);
    } else if(e.keyCode == 82) { // R
        $("#repeat").click();
    }
});

$(window).keyup(function(e){
    if(e.keyCode == 37) {
        $("#rewind").removeClass("active");
    } else if(e.keyCode == 39) {
        $("#forward").removeClass("active");
    }
});

$("#load-song-url").click(function() {
    var url = window.prompt("Song URL:","");

    if(!url) return;

    song_change_count++;

    old_url = $("audio").attr('src');
    old_track = $("#track").html();
    old_artist = $("#artist").html();

    $("audio").attr('src', url);

    updatetime();
    $("#playpause").removeClass("pause");
    $("#albumart").addClass("unknown");

    var track = "Unknown";
    var artist = "Unknown";
    $("#track").html(track);
    $("#artist").html(artist);
    $("#track").attr("title",track);
    $("#artist").attr("title",artist);
});

$("#load-song-file").click(function() {
    $("input[type=file]").click();
});

$("input[type=file]").change(function(e) {
    var file = e.currentTarget.files[0];

    window.URL.revokeObjectURL($("audio").attr('src'));
    song_change_count++;
    var objecturl = URL.createObjectURL(file);
    $("audio").attr("src", objecturl);

    updatetime();
    $("#playpause").removeClass("pause");
    $("#albumart").addClass("unknown");

    track = file.name;
    artist = "Unknown";
    $("#track").html(track);
    $("#artist").html(artist);
    $("#track").attr("title",track);
    $("#artist").attr("title",artist);

    ID3.loadTags(objecturl, function() {
        var tags = ID3.getAllTags(objecturl);

        if (tags.title) {
            $("#track").html(tags.title);
            $("#track").attr("title",tags.title);
        }
        if (tags.artist) {
            $("#artist").html(tags.artist);
            $("#artist").attr("title",tags.artist);
        }
    });
});

$("#plus").click(function() {
    volume(38);
});

$("#minus").click(function() {
    volume(40);
});

$(document).on('mousewheel', function(e){
    if(e.originalEvent.wheelDelta > 0) volume(38);
    else volume(40);
});

$("#albumart").mousedown(function() {
    $("#volume").toggleClass("visible");
});

$("#bg").mousedown(function() {
    $("#volume").removeClass("visible");
});

/* IE */

$("#volume input").on("mousedown touchstart", function() {
    $("#volume").addClass("visible");
});

$("#volume input").on("mouseup touchend", function() {
    $("#volume").removeClass("visible");
});